<?php
require("includes/constants.php");

if (!isset($_SESSION['isLoggedin'])){
    header('location:'.$baseurl);
}else{
    require("includes/config.php");
    require("includes/global_helper.php");
    require("includes/common_helper.php");
    require("includes/auto_number_helper.php");

    include("template/v_header.php");
    include("template/v_nav.php");
    include("template/v_sidebar.php");
    include("template/v_content.php");
    include("template/v_footer.php");
}
?>